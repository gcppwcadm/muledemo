<!DOCTYPE html>
<html ng-app="DemoApp">

<head>
    <title>Apps Orchid</title>
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css" media="screen,projection" />
    <link type="text/css" rel="stylesheet" href="css/animate.min.css" media="screen,projection" />
    <link type="text/css" rel="stylesheet" href="css/custom.css" media="screen,projection" />
    <script src="js/angular.min.js"></script>
<!--
    <script>
    <?php
        echo("URL = 'http://" . gethostbyname('nodejshost') . ":80/users';");
    ?>
    </script>
-->
    <script src="js/main.js"></script>
    <script src="//code.jquery.com/jquery-1.12.1.min.js"></script>
<script src="js/materialize.min.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>

<body ng-controller="appController">
<div class="progress white" style="top:-7px;">
      <div ng-show="showLoading"  class="indeterminate red darken-2"></div>
  </div>
    <div class="container">
        <br>
        <br>
        <h1 class="header center orange-text">Apps Orchid</h1>
        <div class="row center">
            <h5 class="header col s12 light">Employee Record App</h5>
        </div>
        <div class="row">
            <form name="demoForm" ng-submit="getData()">
                <center>
                <div class="input-field" style="width: 500px;">
                    <input id="search" ng-model="reqData" type="text" class="validate">
                    <label for="search">Enter Employee ID (Try entering 1 or 2 or 3)</label>
                </div>
                <div class="input-field">
                    <button class="btn waves-effect waves-light red darken-2" type="submit" name="action">Search
                        <i class="material-icons right">send</i>
                    </button>
                </div>
                </center>
            </form><br><br>
                <div ng-show="resData"class="card grey darken-1" style="width:500px;margin: 0 auto;">
            <div class="card-content white-text">
              <span class="card-title">Employee Details</span><br>
              <span>Employee ID : {{resData.empID}} </span>
    <br>
    <span>Employee Name : {{resData.name}} </span>
    <br>
    <span>Address : {{resData.address}} </span>
    <br>
    <span>Phone No. : {{resData.phone}} </span>
    <br>

            </div>
          </div>
        </div>
        </div>
</body>
</html>
