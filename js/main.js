var app = angular.module('DemoApp', ['appController']);
var appController = angular.module("appController", []);

app.controller('appController', function($scope, $http) {
    $scope.reqData = '';
    $scope.showLoading = false;
    // Change the URL here.
    $scope.getData = function() {
        $scope.showLoading = true;
        var req = { 'empID': $scope.reqData };
        console.log(req);
        $scope.baseURL = "http://nodejshost:80/users/" + $scope.reqData;
        // $scope.baseURL = URL + '/' + $scope.reqData;        
$scope.resData ='';
        $http({
            method: 'POST',
            url: $scope.baseURL,
            data: req
        }).then(function successCallback(response) {
            console.log('Success', response);
            $scope.resData = response.data;
            $scope.showLoading = false;
            if (!response.data)
                Materialize.toast('No Results Found', 4000)
        }, function errorCallback(err) {
            console.log('Error', err);
            $scope.showLoading = false;
            Materialize.toast('No Results Found', 4000)
        });
    }
});
